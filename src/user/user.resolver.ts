import { Args, Query, Resolver, Mutation } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { JwtGuard } from '../auth/jwt.guard';
import { User, CreateUserInput, QueryUserInput, ReturnMessage } from '../graphql.schema';

@Resolver('User')
export class UserResolver {
  constructor(
    private userService: UserService
  ) {}
  @Query('users')
  @UseGuards(JwtGuard)
  async users(@Args('queryUserInput') queryUserInput: QueryUserInput): Promise<User[]> {
    return await this.userService.find(queryUserInput);
  }

  @Query('user')
  @UseGuards(JwtGuard)
  async user(@Args('id') id: string): Promise<User> {
    return await this.userService.findById(id);
  }

  @Mutation('signUp')
  async signUp(@Args('createUserInput') createUserInput: CreateUserInput): Promise<ReturnMessage> {
    return await this.userService.createUser(createUserInput);
  }
}
