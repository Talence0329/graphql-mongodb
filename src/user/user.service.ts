import { Model } from "mongoose";
import { Injectable, InternalServerErrorException, Logger } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { User, CreateUserInput, QueryUserInput, ReturnMessage } from "../graphql.schema";

@Injectable()
export class UserService {
  private logger = new Logger('UserService');

  constructor(@InjectModel("User") private readonly userModel: Model<User>) {}
  /**
   * findById
   * @param id 
   */
  async findById(id: string): Promise<User> {
    try {
      const user = await this.userModel.findById(id).exec();
      return user;
    } catch (error) {
      this.logger.error(`Failed to get user for user "${id}". Filters: ${id}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * find
   * @param queryUserInput 
   */
  async find(queryUserInput: QueryUserInput): Promise<User[]> {
    try {
      const users = await this.userModel.find(queryUserInput).exec();
      return users;
    } catch (error) {
      this.logger.error(`Failed to get users for user "${queryUserInput.email}". Filters: ${JSON.stringify(queryUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * findOne
   * @param queryUserInput 
   */
  async findOne(queryUserInput: QueryUserInput): Promise<User> {
    try {
      const user = await this.userModel.findOne(queryUserInput).exec();
      return user;
    } catch (error) {
      this.logger.error(`Failed to get user for user "${queryUserInput.email}". Filters: ${JSON.stringify(queryUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * createUser
   * @param createUserInput 
   */
  async createUser(createUserInput: CreateUserInput): Promise<ReturnMessage | InternalServerErrorException> {
    try {
      const createdUser = new this.userModel(createUserInput);
      await createdUser.save();
      return { path: 'createUser', message: 'OK' };
    } catch (error) {
      this.logger.error(`Failed to add user for user "${createUserInput.email}". Filters: ${JSON.stringify(createUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }
}
