import * as mongoose from "mongoose";
import { generate, isHashed } from 'password-hash';

export const UserSchema = new mongoose.Schema({
  userName: { type: String },
  email: { type: String, createIndexes: true, unique: true },
  password: { type: String },
  createAt: { type: Date, default: Date.now },
  updateAt: { type: Date, default: Date.now }
})

UserSchema.pre("save", function(next) {
  if (!isHashed(this.password)) {
    this.password = generate(this.password);
  }
  if (this.isNew) {
    this.createAt = new Date();
  } else {
    this.updateAt = new Date();
  }
  next()
})
