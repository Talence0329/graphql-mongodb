
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class CreateUserInput {
    userName: string;
    email: string;
    password: string;
}

export class QueryUserInput {
    userName?: string;
    email?: string;
}

export class SignInInput {
    email: string;
    password: string;
}

export abstract class IMutation {
    abstract signIn(signInInput: SignInInput): Token | Promise<Token>;

    abstract signUp(createUserInput: CreateUserInput): ReturnMessage | Promise<ReturnMessage>;
}

export abstract class IQuery {
    abstract user(id: string): User | Promise<User>;

    abstract users(queryUserInput: QueryUserInput): User[] | Promise<User[]>;
}

export class ReturnMessage {
    path?: string;
    message?: string;
}

export class Token {
    accessToken?: string;
    message?: string;
}

export class User {
    id: string;
    userName: string;
    email: string;
    password: string;
    createAt?: Date;
    updateAt?: Date;
}
