import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import * as config from 'config';

const dbConfig = config.get('db');

@Module({
  imports: [
    AuthModule,
    UserModule,
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
      context: ({ req }) => ({ req })
    }),
    MongooseModule.forRoot(dbConfig.uri, dbConfig.options)
  ]
})
export class AppModule {}
