import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import * as config from 'config';

async function bootstrap() {
  const logger = new Logger('Bootstrap');
  const serverConfig = config.get('server');
  const port = process.env.PORT || serverConfig.port;
  logger.log(`GraphQL Server bootstrap NODE_ENV="${process.env.NODE_ENV}"`);

  const app = await NestFactory.create(AppModule, {
    logger: serverConfig.logger
  });
  await app.listen(port);
  logger.log(`Application listening on port ${port}`);
}
bootstrap();
