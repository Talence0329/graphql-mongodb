import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { verify } from 'password-hash';
import { UserService } from '../user/user.service';
import { SignInInput, Token, User } from '../graphql.schema';

@Injectable()
export class AuthService {
  private logger = new Logger('AuthService');

  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) { }

  /**
   * 登入
   * @param signInInput 
   */
  async signIn(signInInput: SignInInput): Promise<Token> {
    try {
      const user = await this.userService.findOne({ email: signInInput.email });
      if (!user) {
        this.logger.debug("#1. invalid email or password");
        return { message: "invalid email or password" };
      }
      if (!verify(signInInput.password, user.password)) {
        this.logger.debug("#2. invalid email or password");
        return { message: "invalid email or password" };
      }
      const token = await this.signToken(user);
      return { accessToken: token };
    } catch (error) {
      this.logger.error(`Failed to signUp for user "${signInInput.email}". Filters: ${JSON.stringify(signInInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * 產生token
   * @param user 
   */
  async signToken(user: User): Promise<string> {
    try {
      const payload = { username: user.userName, sub: user.email };
      return this.jwtService.sign(payload);
    } catch (error) {
      this.logger.error(`Failed to signUp for user "${user.email}". Filters: ${JSON.stringify(user)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }
}
