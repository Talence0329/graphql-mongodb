import { ExecutionContext, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';

export class JwtGuard extends AuthGuard('jwt') {
  private logger = new Logger('JwtGuard');

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    this.logger.debug(ctx.getContext().req.body);
    return ctx.getContext().req;
  }
}
