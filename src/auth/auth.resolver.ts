import { Args, Resolver, Mutation } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { SignInInput,Token} from '../graphql.schema';
  
@Resolver('Auth')
export class AuthResolver {
  constructor(
    private authService: AuthService
  ) { }

  @Mutation('signIn')
  async signIn(@Args('signInInput') signInInput: SignInInput): Promise<Token> {
    return await this.authService.signIn(signInInput);
  }
}
