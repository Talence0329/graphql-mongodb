## 設定Mongodb
[Mongodb本地測試環境前置動作](MONGODB_PREPARE.md) 已有環境者不需執行此流程

此流程需搭配GraphQL進行，請先確定GraphQL已建置完畢

流程結束後，可確實連線至mongodb並異動資料表與資料

### **載入Mongoose並連線資料庫**
修改"app.module.ts"檔案

*src/app.module.ts*
```typescript
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    UserModule,
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true
    }),
    MongooseModule.forRoot('mongodb://talence:123456@localhost:27017/test?retryWrites=true&w=majority', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
  ]
})
export class AppModule {}
```

MongooseModule.forRoot("連線字串", options)

"連線字串"須依想連線的資料庫作改變

此範例連線字串為：連線 localhost:27017 的 test 資料庫 使用者talence / 123456

---

### **設定user的schema**
修改"user.graphql"檔案

*src/user/user.graphql*
```graphql
scalar Date

type User {
  id: String!
  userName: String!
  email: String!
  password: String!
  createAt: Date
  updateAt: Date
}

input CreateUserInput {
  userName: String!
  email: String!
  password: String!
}

input QueryUserInput {
  userName: String
  email: String   
}

type Query {
  user(id: String!): User
  users(queryUserInput: QueryUserInput!): [User]
}

type Mutation {
  signUp(createUserInput: CreateUserInput!): Token
}
```

建立"user.schema.ts"檔案

*src/user/user.schema.ts*
```typescript
import * as mongoose from "mongoose";
import { generate,isHashed } from 'password-hash';

export const UserSchema = new mongoose.Schema({
  userName: { type: String },
  email: { type: String,createIndexes: true, unique: true  },
  password: { type: String },
  createAt: { type: Date, default: Date.now },
  updateAt: { type: Date }
})

UserSchema.pre("save", function(next) {
  if (!isHashed(this.password)) {
    this.password = generate(this.password);
  }
  if (this.isNew) {
    this.createAt = new Date();
  } else {
    this.updateAt = new Date();
  }
  next()
})
```

修改"user.module.ts"檔案

*src/user/user.module.ts*
```typescript
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './user.schema';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])],
  exports: [UserService,],
  providers: [UserService, UserResolver],
})
export class UserModule {}
```

修改"user.serveice.ts"檔案

*src/user/user.serveice.ts*
```typescript
import { Model } from "mongoose";
import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { User, CreateUserInput, QueryUserInput, ReturnMessage } from "../graphql.schema";

@Injectable()
export class UserService {
  constructor(@InjectModel("User") private readonly userModel: Model<User>) {}
  /**
   * findById
   * @param id 
   */
  async findById(id: string): Promise<User> {
    try {
      const user = await this.userModel.findById(id).exec();
      return user;
    } catch (error) {
      console.error(`Failed to get user for user "${id}". Filters: ${id}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * find
   * @param queryUserInput 
   */
  async find(queryUserInput: QueryUserInput): Promise<User[]> {
    try {
      const users = await this.userModel.find(queryUserInput).exec();
      return users;
    } catch (error) {
      console.error(`Failed to get users for user "${queryUserInput.email}". Filters: ${JSON.stringify(queryUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * findOne
   * @param queryUserInput 
   */
  async findOne(queryUserInput: QueryUserInput): Promise<User> {
    try {
      const user = await this.userModel.findOne(queryUserInput).exec();
      return user;
    } catch (error) {
      console.error(`Failed to get user for user "${queryUserInput.email}". Filters: ${JSON.stringify(queryUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * createUser
   * @param createUserInput 
   */
  async createUser(createUserInput: CreateUserInput): Promise<ReturnMessage | InternalServerErrorException> {
    try {
      const createdUser = new this.userModel(createUserInput);
      await createdUser.save();
      return { path: 'createUser', message: 'OK' };
    } catch (error) {
      console.error(`Failed to add user for user "${createUserInput.email}". Filters: ${JSON.stringify(createUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }
}
```

修改"user.resolver.ts"

*src/user/user.resolver.ts*
```typescript
import { Args, Query, Resolver, Mutation } from '@nestjs/graphql';
import { UserService } from './user.service';
import { User, CreateUserInput, QueryUserInput, ReturnMessage } from '../graphql.schema';

@Resolver('User')
export class UserResolver {
  constructor(
    private userService: UserService
  ) {}
  @Query('users')
  async users(@Args('queryUserInput') queryUserInput: QueryUserInput): Promise<User[]> {
    return await this.userService.find(queryUserInput);
  }

  @Query('user')
  async user(@Args('id') id: string): Promise<User> {
    return await this.userService.findById(id);
  }

  @Mutation('signUp')
  async signUp(@Args('createUserInput') createUserInput: CreateUserInput): Promise<ReturnMessage> {
    return await this.userService.createUser(createUserInput);
  }
}
```

---

### **測試：**

更新"graphql.schema.ts"檔案(如原先沒有也會自動生成)
```shell
npm run prebuild
```
啟動服務
```shell
npm run start
```
開啟網頁

http://localhost:3000/graphql

**新增使用者** 

在query區域輸入
```graphql
mutation {
  signUp (createUserInput: {userName: "test", email: "test@example.com", password: "123456"}) {
    path
    message
  }
}
```
點擊中間按鈕後應出現結果如下圖

![結果1](./img/mongodb-result1.png)

**查詢使用者** 

在query區域輸入
```graphql
query {
  users (queryUserInput: {userName: "test"}) {
    userName
    email
    password
  }
}
```
點擊中間按鈕後應出現結果如下圖

![結果2](./img/mongodb-result2.png)

[Mongoose 查詢、model操作](https://mongoosejs.com/docs/api/query.html#query_Query-find)

完成設定Mongodb後接續[設定JWT](JWT.md)
