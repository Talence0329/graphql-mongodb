## 設定GraphQL
進行完此流程即有基本的GraphQL架構，可依需求接續其他的設定

### **建立產生graphql.schema.ts機制**
設定"package.json"的scripts 加入/修改"prebuild"、"generate"指令

*package.json*
```json
"script": {
  "prebuild": "npm run generate",
  "generate": "tsc ./src/generate-typings --outDir ./dist & node ./dist/generate-typings",
  ...
}
```

於"src"資料夾建立"generate-typings.ts"檔案

*src/generate-typings.ts*
```typescript
import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: ['./src/**/*.graphql'],
  path: join(process.cwd(), './src/graphql.schema.ts'),
  outputAs: 'class',
});
```

之後只要運行 ```npm run prebuild``` 即會生成"graphql.schema.ts"檔案供引入schema定義的type

---

### **設定自動載入graphql檔案的目錄範圍**
修改"app.module.ts"

*src/app.module.ts*
```typescript
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

@Module({
  imports: [
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true
    })
  ]
})
export class AppModule {}
```

---

### **設定通用type(scalar)：**
設定於"scalar"的type皆可以於各.graphql檔案中直接使用

於"src"建立資料夾"common"並於其中建立資料夾"scalars"

建立"date.scalar.ts"

*src/common/scalars/date.scalar.ts*
```typescript
import { CustomScalar, Scalar } from '@nestjs/graphql';
import { Kind } from 'graphql';

@Scalar('Date')
export class DateScalar implements CustomScalar<number, Date> {
  description = 'Date custom scalar type';

  parseValue(value: number): Date {
    return new Date(value); // value from the client
  }

  serialize(value: Date): number {
    return value.getTime(); // value sent to the client
  }

  parseLiteral(ast: any): Date {
    if (ast.kind === Kind.INT) {
      return new Date(ast.value);
    }
    return null;
  }
}
```
建立"errorMessage.ts"

*src/common/scalars/建立errorMessage.ts*
```typescript
export const errorMessage = (path: string, message: string) => [
  {
    path,
    message,
  }
];

```

---

### **建立user：**

執行指令以產生檔案
```shell
nest g module user
nest g resolver user
nest g service user
```
*可以將"\*.spec.ts"檔案刪除，不影響程式*

建立"user.graphql"檔案，此為user的schema

*src/user/user.graphql*
```graphql
scalar Date

type User {
  id: String!
  userName: String!
  email: String!
  password: String!
  createAt: Date
  updateAt: Date
}

type Query {
  user(id: String!): User
}
```

修改"user.service.ts"檔案，主要由此進行與資料庫的互動(*此範例使用固定資料作為示範*)

*src/user/user.service.ts*
```typescript
import {
  Injectable,
  InternalServerErrorException
} from "@nestjs/common";
import { User } from "../graphql.schema";

@Injectable()
export class UserService {
  async findById(id: string): Promise<User> {
    try {
      const user = new User()
      user.id = id
      user.userName = 'test'
      user.email = 'test@example.com'
      return user;
    } catch (error) {
      console.error(`Failed to get user for user "${id}". Filters: ${id}`, error.stack);
      throw new InternalServerErrorException();
    }
  }
}
```

修改"user.resolver.ts"檔案，主要由此進行指令的導向

*src/user/user.resolver.ts*
```typescript
import { Args, Query, Resolver } from '@nestjs/graphql';
import { UserService } from './user.service';
import { User } from '../graphql.schema';

@Resolver('User')
export class UserResolver {
  constructor(
    private userService: UserService
  ) { }

  @Query('user')
  async user(@Args('id') id: string): Promise<User> {
    return await this.userService.findById(id);
  }
}
```

---

### **測試：**

更新"graphql.schema.ts"檔案(如原先沒有也會自動生成)
```shell
npm run prebuild
```
啟動服務
```shell
npm run start
```
開啟網頁

http://localhost:3000/graphql

在query區域輸入
```graphql
query {
  user (id: "1") {
    id
    userName
    email
  }
}
```
點擊中間按鈕後應出現結果如下圖

![結果](./img/graphql-result.png)

完成設定GraphQL後接續[設定Mongodb](MONGODB.md)
