## 設定Logger

Nest.js內建的Logger功能，可以產生紀錄

### **將log機制改為Logger**

修改"main.ts"檔案

*main.ts*
```typescript
import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import * as config from 'config';

async function bootstrap() {
  const logger = new Logger('Bootstrap');
  const serverConfig = config.get('server');
  const port = process.env.PORT || serverConfig.port;
  logger.log(`GraphQL Server bootstrap NODE_ENV="${process.env.NODE_ENV}"`);

  const app = await NestFactory.create(AppModule, {
    logger: serverConfig.logger
  });
  await app.listen(port);
  logger.log(`Application listening on port ${port}`);
}
bootstrap();
```

修改"user.service.ts"檔案

*src/user/user.service.ts*
```typescript
import { Model } from "mongoose";
import { Injectable, InternalServerErrorException, Logger } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { User, CreateUserInput, QueryUserInput, ReturnMessage } from "../graphql.schema";

@Injectable()
export class UserService {
  private logger = new Logger('UserService');

  constructor(@InjectModel("User") private readonly userModel: Model<User>) {}
  /**
   * findById
   * @param id 
   */
  async findById(id: string): Promise<User> {
    try {
      const user = await this.userModel.findById(id).exec();
      return user;
    } catch (error) {
      this.logger.error(`Failed to get user for user "${id}". Filters: ${id}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * find
   * @param queryUserInput 
   */
  async find(queryUserInput: QueryUserInput): Promise<User[]> {
    try {
      const users = await this.userModel.find(queryUserInput).exec();
      return users;
    } catch (error) {
      this.logger.error(`Failed to get users for user "${queryUserInput.email}". Filters: ${JSON.stringify(queryUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * findOne
   * @param queryUserInput 
   */
  async findOne(queryUserInput: QueryUserInput): Promise<User> {
    try {
      const user = await this.userModel.findOne(queryUserInput).exec();
      return user;
    } catch (error) {
      this.logger.error(`Failed to get user for user "${queryUserInput.email}". Filters: ${JSON.stringify(queryUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * createUser
   * @param createUserInput 
   */
  async createUser(createUserInput: CreateUserInput): Promise<ReturnMessage | InternalServerErrorException> {
    try {
      const createdUser = new this.userModel(createUserInput);
      await createdUser.save();
      return { path: 'createUser', message: 'OK' };
    } catch (error) {
      this.logger.error(`Failed to add user for user "${createUserInput.email}". Filters: ${JSON.stringify(createUserInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }
}
```

修改"auth.service.ts"檔案

*src/auth/auth.service.ts*
```typescript
import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { verify } from 'password-hash';
import { UserService } from '../user/user.service';
import { SignInInput, Token, User } from '../graphql.schema';

@Injectable()
export class AuthService {
  private logger = new Logger('AuthService');

  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) { }

  /**
   * 登入
   * @param signInInput 
   */
  async signIn(signInInput: SignInInput): Promise<Token> {
    try {
      const user = await this.userService.findOne({ email: signInInput.email });
      if (!user) {
        this.logger.debug("#1. invalid email or password");
        return { message: "invalid email or password" };
      }
      if (!verify(signInInput.password, user.password)) {
        this.logger.debug("#2. invalid email or password");
        return { message: "invalid email or password" };
      }
      const token = await this.signToken(user);
      return { accessToken: token };
    } catch (error) {
      this.logger.error(`Failed to signUp for user "${signInInput.email}". Filters: ${JSON.stringify(signInInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * 產生token
   * @param user 
   */
  async signToken(user: User): Promise<string> {
    try {
      const payload = { username: user.userName, sub: user.email };
      return this.jwtService.sign(payload);
    } catch (error) {
      this.logger.error(`Failed to signUp for user "${user.email}". Filters: ${JSON.stringify(user)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }
}
```

修改"auth.strategy.ts"檔案

*src/auth/auth.strategy.ts*
```typescript
import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import * as config from 'config';

const jwtConfig = config.get('jwt');

@Injectable()
export class AuthStrategy extends PassportStrategy(Strategy) {
  private logger = new Logger('AuthStrategy');
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET || jwtConfig.secret
    });
  }

  async validate(payload: any) {
    this.logger.debug(payload);
    return { email: payload.sub, username: payload.username };
  }
}
```

修改"jwt.guard.ts"檔案

*src/auth/jwt.guard.ts*
```typescript
import { ExecutionContext, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';

export class JwtGuard extends AuthGuard('jwt') {
  private logger = new Logger('JwtGuard');

  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    this.logger.debug(ctx.getContext().req.body);
    return ctx.getContext().req;
  }
}
```