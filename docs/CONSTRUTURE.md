# 建構流程
1. 安裝套件
2. 設定GraphQL
3. 設定Mongodb
4. 設定JWT
5. 設定Config
6. 設定Logger

## 1. 安裝套件
GraphQL

```shell
npm i --save @nestjs/graphql graphql-tools graphql graphql-subscriptions
```

Mongoose

```shell
npm install --save @nestjs/mongoose mongoose
```

登入驗證

```shell
npm install --save @nestjs/passport passport
npm install @nestjs/jwt passport-jwt
npm install @types/passport-jwt --save-dev
```
config

```shell
npm install config
```

加密

```shell
npm install password-hash
```
---

## 2. 設定GraphQL
[設定GraphQL](GRAPHQL.md)

---

## 3. 設定Mongodb
[設定Mongodb](MONGODB.md)

---

## 4. 設定JWT
[設定JWT](JWT.md)

---

## 5. 設定Config
[設定Config](CONFIG.md)

---

## 6. 設定Logger
[設定Logger](LOGGER.md)