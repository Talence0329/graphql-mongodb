## 設定JWT

此流程會建立JWT驗證機制，需搭配GraphQL以及Mongodb

### 建立auth

執行指令以產生檔案
```shell
nest g module auth
nest g resolver auth
nest g service auth
```
*可以將"\*.spec.ts"檔案刪除，不影響程式*

建立"auth.graphql"檔案，此為auth的schema

*src/auth/auth.graphql*
```graphql
scalar Date
 
type Token {
  accessToken: String
  message: String
}

input SignInInput {
  email: String!
  password: String!
}

type Mutation {
  signIn(signInInput: SignInInput!): Token
}
```

修改"auth.module.ts"檔案

*src/auth/auth.module.ts*
```typescript
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';
import { AuthStrategy } from './auth.strategy';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: 'secretKey',
      signOptions: {
        expiresIn: '60s',
      },
    }),
  ],
  providers: [AuthResolver, AuthService, AuthStrategy]
})
export class AuthModule {}
```

修改"auth.service.ts"檔案

*src/auth/auth.service.ts*
```typescript
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { verify } from 'password-hash';
import { UserService } from '../user/user.service';
import { SignInInput, Token, User } from '../graphql.schema';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) { }

  /**
   * 登入
   * @param signInInput 
   */
  async signIn(signInInput: SignInInput): Promise<Token> {
    try {
      const user = await this.userService.findOne({ email: signInInput.email });
      if (!user) {
        console.debug("#1. invalid email or password");
        return { message: "invalid email or password" };
      }
      if (!verify(signInInput.password, user.password)) {
        console.debug("#2. invalid email or password");
        return { message: "invalid email or password" };
      }
      const token = await this.signToken(user);
      return { accessToken: token };
    } catch (error) {
      console.error(`Failed to signUp for user "${signInInput.email}". Filters: ${JSON.stringify(signInInput)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }

  /**
   * 產生token
   * @param user 
   */
  async signToken(user: User): Promise<string> {
    try {
      const payload = { username: user.userName, sub: user.email };
      return this.jwtService.sign(payload);
    } catch (error) {
      console.error(`Failed to signUp for user "${user.email}". Filters: ${JSON.stringify(user)}`, error.stack);
      throw new InternalServerErrorException();
    }
  }
}
```

建立"auth.strategy.ts"檔案

*src/auth/auth.strategy.ts*
```typescript
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class AuthStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'secretKey'
    });
  }

  async validate(payload: any) {
    return { email: payload.sub, username: payload.username };
  }
}
```

建立"jwt.guard.ts"檔案

*src/auth/jwt.guard.ts*
```typescript
import { ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';

export class JwtGuard extends AuthGuard('jwt') {
  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }
}
```

---

### **修改app.module.ts**

*src/app.module.ts*
```typescript
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    AuthModule,
    UserModule,
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
      context: ({ req }) => ({ req })
    }),
    MongooseModule.forRoot('mongodb://talence:123456@localhost:27017/test?retryWrites=true&w=majority',{
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    })
  ]
})
export class AppModule {}
```

---

### 測試：

更新"graphql.schema.ts"檔案(如原先沒有也會自動生成)
```shell
npm run prebuild
```
啟動服務
```shell
npm run start
```
開啟網頁

http://localhost:3000/graphql

**未使用token進行query**

在query區域輸入
```graphql
query {
  user (id: "1") {
    id
    userName
    email
  }
}
```
點擊中間按鈕後應出現未授權錯誤，結果如下圖

![結果1](./img/jwt-result1.png)

**登入取得token**

在query區域輸入
```graphql
mutation {
  signIn (signInInput: {email: "test@example.com", password: "123456"}) {
    message
    accessToken
  }
}
```
點擊中間按鈕後應出現token，結果如下圖

![結果2](./img/jwt-result2.png)

**使用token進行query**

複製"accessToken"的值

點擊左下的"HTTP HEADERS"出現區塊

![結果3](./img/jwt-result3.png)

於區塊中輸入以下內容
```json
{
  "Authorization": "Bearer 方才複製的token"
}
```

![結果4](./img/jwt-result4.png)

在query區域輸入
```graphql
query {
  user (id: "1") {
    id
    userName
    email
  }
}
```
點擊中間按鈕後應出現未授權錯誤，結果如下圖

![結果5](./img/jwt-result5.png)

完成設定JWT後接續[設定Config](CONFIG.md)
