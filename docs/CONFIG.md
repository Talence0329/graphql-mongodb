## 設定Config

使用config全域設定參數

### **建立config資料**

於根目錄新增"config"資料夾

新增"default.yml"檔案

*config/default.yml*
```yml
server:
  port: 3000
  logger: ['log', 'error', 'warn', 'debug']
db:
  uri: "mongodb://talence:123456@localhost:27017/test?retryWrites=true&w=majority"
  options: { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }
jwt:
  expiresIn: 3600
  secret: 'secretKey'
```

新增"development.yml"檔案

*config/development.yml*
```yml
server:
  logger: ['log', 'error', 'warn', 'debug']
```

新增"production.yml"檔案

*config/production.yml*
```yml
server:
  port: 3000
  logger: ['log', 'error', 'warn']
db:
  uri: "mongodb://talence:123456@localhost:27017/test?retryWrites=true&w=majority"
```

---

### **將參數替換為config提供**

修改"main.ts"檔案

*main.ts*
```typescript
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as config from 'config';

async function bootstrap() {
  const serverConfig = config.get('server');
  const port = process.env.PORT || serverConfig.port;

  const app = await NestFactory.create(AppModule, {
    logger: serverConfig.logger
  });
  await app.listen(port);
}
bootstrap();
```

修改"app.module.ts"檔案

*src/app.module.ts*
```typescript
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import * as config from 'config';

const dbConfig = config.get('db');

@Module({
  imports: [
    AuthModule,
    UserModule,
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
      context: ({ req }) => ({ req })
    }),
    MongooseModule.forRoot(dbConfig.uri, dbConfig.options)
  ]
})
export class AppModule {}
```

修改"auth.module.ts"檔案

*src/auth/auth.module.ts*
```typescript
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';
import { AuthStrategy } from './auth.strategy';
import { UserModule } from '../user/user.module';
import * as config from 'config';

const jwtConfig = config.get('jwt');

@Module({
  imports: [
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET || jwtConfig.secret,
      signOptions: {
        expiresIn: jwtConfig.expiresIn,
      },
    }),
  ],
  providers: [AuthResolver, AuthService, AuthStrategy]
})
export class AuthModule {}
```

修改"auth.strategy.ts"檔案

*src/auth/auth.strategy.ts*
```typescript
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import * as config from 'config';

const jwtConfig = config.get('jwt');

@Injectable()
export class AuthStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET || jwtConfig.secret
    });
  }

  async validate(payload: any) {
    return { email: payload.sub, username: payload.username };
  }
}
```

完成設定Config後接續[設定Logger](LOGGER.md)
