## Mongodb建置流程

1. 安裝mongodb [官方下載網站](https://www.mongodb.com/download-center/community)
2. 安裝Studio 3T [官方下載網站](https://studio3t.com/download/)
3. 開啟Studio 3T並點擊Connect ![建立連線](./img/mongodb_prepare_1.png)
4. 建立新連線(步驟如圖) ![建立新連線](./img/mongodb_prepare_2.png)
5. 建立資料庫
  ![建立資料庫](./img/mongodb_1.png)
  ![建立資料庫2](./img/mongodb_2.png)
6. 建立使用者
  ![建立使用者](./img/mongodb_3.png)
  ![建立使用者2](./img/mongodb_4.png)