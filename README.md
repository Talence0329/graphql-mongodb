# API後端基礎架構

## 說明

將作為未來開新專案時使用的基礎架構

---

## 架構

- API架構：GraphQL
- 資料庫：Mongodb
- 驗證機制：JWT
- 全域設定參數：config
- 歷程記錄：Logger

---

## 使用說明

先執行指令進行安裝
```
npm install
```
執行指令啟動服務
```
npm run start
```
開啟 http://localhost:3000/graphql 可進行測試

### 基本query方式

取得指定user(id = 1)的userName, email
```graphql
query {
  user (id: "1") {
    id
    userName
    email
  }
}
```
結果(json格式)
```json
{
  "data": {
    "user": {
      "userName": "test",
      "email": "test@example.com"
    }
  }
}
```

---

## 建構流程
1. 安裝套件
2. 設定GraphQL
3. 設定Mongodb
4. 設定JWT
5. 設定Config
6. 設定Logger

本專案的詳細建構流程可參考[建構流程](/docs/CONSTRUTURE.md)